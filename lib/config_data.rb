# frozen_string_literal: true

require 'config_data/version'
require 'forwardable'
require 'yaml'

#
module Manifesto
  #
  module Utils
    #
    module ConfigData
      class << self
        def extended(subklass)
          prep_subklass(subklass)
        end

        def included(subklass)
          prep_subklass(subklass)
        end

        def determine_filename(klass)
          if klass.respond_to?(:filename)
            klass.filename
          elsif direct_objects.find { |obj| klass.instance_of?(obj) }
            Where.is(klass)[:file]
          else
            determine_filename(klass.class)
          end
        end

        private

        def direct_objects
          [Class, Module]
        end

        def prep_subklass(subklass)
          subklass.extend ModuleMethods
          subklass.include InstanceMethods
        end
      end

      #
      module ModuleMethods
        def config_data(file: ConfigData.determine_filename(self))
          ConfigDatum.new(ConfigFile.load_yaml(file: file))
        end

        def config_raw_data(file: ConfigData.determine_filename(self))
          ConfigFile.load(file: file)
        end
      end

      #
      module InstanceMethods
        def config_data(file: ConfigData.determine_filename(self))
          self.class.config_data(file: file)
        end
      end
    end

    #
    module ConfigFile
      class << self
        def load(file:)
          file_data = ::IO.binread(file)
          data = file_data.split(/^__END__$/, 2).last
          return if data&.strip!&.empty?
          data
        end

        def load_yaml(file:)
          raw_data = load(file: file)
          return if raw_data.nil?
          YAML.safe_load(raw_data)
        end
      end
    end

    #
    class ConfigDatum
      extend Forwardable
      def_delegators :@datum, :[]

      def initialize(datum)
        @datum = datum || {}
      end

      def to_h
        @datum
      end
    end
  end
end
