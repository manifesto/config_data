# frozen_string_literal: true

require 'singleton'
require 'pry'

module C
  include Manifesto::Utils::ConfigData

  def method_a; end
end
__END__
---
c:
