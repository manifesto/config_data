require 'singleton'

class B
  include Manifesto::Utils::ConfigData
  include Singleton

  class << self
    def filename
      Where.is(self, ignore: /singleton/)[:file]
    end

    def class_a
      puts 'hi'
    end
  end

  def instance_a; end
end
__END__
---
b:
