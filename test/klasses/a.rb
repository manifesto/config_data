# frozen_string_literal: true

require 'singleton'
require 'pry'

class A
  include Manifesto::Utils::ConfigData

  def instance_a; end
end
__END__
---
a:
