# frozen_string_literal: true

require 'bundler'

Bundler.require

require 'minitest/autorun'

require_relative 'klasses/a'
require_relative 'klasses/b'
require_relative 'klasses/c'
