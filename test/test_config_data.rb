# frozen_string_literal: true

require_relative 'helper'

#
class TestConfigData < MiniTest::Test
  def test_config_data
    assert_equal [{ 'a' => nil }], [A.new.config_data.to_h]
  end

  def test_singleton_config_data
    assert_equal [{ 'b' => nil }], [B.instance.config_data.to_h]
  end
end

#
class TestClassConfigData < MiniTest::Test
  def test_config_data
    assert_equal [{ 'a' => nil }], [A.config_data.to_h]
  end

  def test_singleton_config_data
    assert_equal [{ 'b' => nil }], [B.config_data.to_h]
  end
end

#
class TestModuleConfigData < MiniTest::Test
  def test_module_config_data
    assert_equal [{ 'c' => nil }], [C.config_data.to_h]
  end
end
